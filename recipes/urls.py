from recipes.views import RecipeListView
from django.urls import path

urlpatterns = [
    path('recipes/list.html/', RecipeListView.as_view(), name="list_of_all_recipes")
]

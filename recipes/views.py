from django.shortcuts import render
from django.views.generic.list import ListView
from recipes.models import Recipe


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"

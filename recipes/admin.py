from django.contrib import admin
from recipes.models import Recipe, Measure, FoodItem, Instruction

admin.site.register(Recipe)
admin.site.register(Measure)
admin.site.register(FoodItem)
admin.site.register(Instruction)

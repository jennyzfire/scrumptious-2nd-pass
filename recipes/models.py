from django.db import models

class Recipe(models.Model):
    name = models.CharField(max_length=120)
    description = models.TextField()
    created = models.DateTimeField(auto_now_add=False)
    edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.name}"


class Instruction(models.Model):
    fooditem = models.ForeignKey(
        "Recipe",
        related_name="recipes",
        on_delete=models.CASCADE,
        null=True
    )
    steps = models.TextField()

    def __str__(self):
        return f"{self.fooditem}"


class FoodItem(models.Model):
    name = models.CharField(max_length=120)

    def __str__(self):
        return f"{self.name}"


class Measure(models.Model):
    amount = models.DecimalField(decimal_places=2, max_digits=5)
    units = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.amount} {self.units} of "
